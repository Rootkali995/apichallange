<?php

namespace App\Http\Controllers\Api\V1;

use App\Model\Product;
use App\User;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function getProducts()
    {
        $data = Product::with('user')->get()
            ->transform(function ($item) {
                return [
                    "type" => "products",
                    "id" => $item->id,
                    "attributes" => [
                        "name" => $item->name,
                        "model" => $item->model,
                        "sku" => $item->sku,
                        "total_qty" => $item->total_qty,
                        "cost" => $item->cost,
                        "weight" => $item->weight,
                        "length" => $item->length,
                        "created_at" => $item->created_at->format('Y-m-d'),
                        "updated_at" => $item->updated_at->format('Y-m-d')
                    ],
                    "relationships" => [
                        'users' => [
                            'data' => [
                                [
                                    "type" => "users",
                                    "id" => $item->user->id,
                                ]
                            ]]
                    ]];
            });

        $included = User::all()
        ->transform(function($item) {
            return [
                'type' => 'users',
                'id' => $item->id,
                'attributes' => [
                    'name' => $item->name
                ],
            ];
        });

        return response()->json(compact('data', 'included'));
    }
}
