<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'total_qty' => 'string'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
