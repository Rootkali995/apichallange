<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content" id="root">
                <table class="table">
                    <tr>
                        <th>id</th>
                        <th>name</th>
                    </tr>
                    <tr v-for="val in items.data">
                        <td>@{{ val.id }}</td>
                        <td>@{{ val.attributes.name }}</td>
                    </tr>
                </table>
                <table class="table">
                    <tr>
                        <th>id</th>
                        <th>name</th>
                    </tr>
                    <tr v-for="val in items.included">
                        <td>@{{ val.id }}</td>
                        <td>@{{ val.type }}</td>
                    </tr>
                </table>
            </div>
        </div>
    <footer>
        <script src="{{ asset('js/app.js') }}"></script>
    </footer>
    </body>
</html>
