# Garganto Tech Programming Challenge

We are looking for Senior / Junior PHP Developers who has a passion for E-Commerce and are familiar or highly interested with building API.

# What do you need to do?

The idea here is to built a REST API utilizing the sample data (imported to a database) provided with any of the currently tending PHP frameworks (Laravel, Symfony, Lumen, Silex, Yii) or even plain PHP if that is what to your fancy.

The only requirement is that it should have at least one endpoint with similar or exactly the same data structure to data.json. You do not need to stop there though and feel free to build something with some the hottest frontend framework like AngularJS / VueJS / ReactJS or even plain old trusty jQuery to score some brownie points.

# How do you do it?

1. Fork this repository.
2. Start coding and let your creativity run wild.
3. Submit a pull request when you're done!
4. We'll review your code and get back to you.

