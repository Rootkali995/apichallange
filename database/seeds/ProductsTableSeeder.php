<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "name" => "Voluptatum similique ullam molestias aut.",
                "model" => "AID 34567",
                "sku" => "AID-34567",
                "total_qty" => "3",
                "cost" => "90.00",
                "weight" => "1kg",
                "length" => "100cm",
                "user_id" => 1,
                "created_at" => date('Y-m-d'),
                "updated_at" => date('Y-m-d')
            ],
            [
                "name" => "Quis maiores inventore omnis.",
                "model" => "BCC 34567",
                "sku" => "BCC-34567",
                "total_qty" => "5",
                "cost" => "19.90",
                "weight" => "1kg",
                "length" => "100cm",
                "user_id" => 1,
                "created_at" => date('Y-m-d'),
                "updated_at" => date('Y-m-d')
            ],
            [
                "name" => "Est libero voluptatem quos.",
                "model" => "DDF 34567",
                "sku" => "DDF-34567",
                "total_qty" => "9",
                "cost" => "5.35",
                "weight" => "1kg",
                "length" => "100cm",
                "user_id" => 1,
                "created_at" => date('Y-m-d'),
                "updated_at" => date('Y-m-d')
            ],
        ];

        DB::table('products')->insert($data);
    }
}
