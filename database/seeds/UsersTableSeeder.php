<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "id" => 1,
                "name" => "Merchant 1",
                "email" => "Merchant_1@gmail.com",
                "password" => bcrypt("test"),
                "created_at" => date('Y-m-d'),
                "updated_at" => date('Y-m-d')
            ],
            [
                "id" => 2,
                "name" => "Merchant 2",
                "email" => "Merchant_2@gmail.com",
                "password" => bcrypt("test"),
                "created_at" => date('Y-m-d'),
                "updated_at" => date('Y-m-d')
            ],
            [
                "id" => 3,
                "name" => "Merchant 3",
                "email" => "Merchant_3@gmail.com",
                "password" => bcrypt("test"),
                "created_at" => date('Y-m-d'),
                "updated_at" => date('Y-m-d')
            ],
        ];

        DB::table('users')->insert($data);
    }
}
